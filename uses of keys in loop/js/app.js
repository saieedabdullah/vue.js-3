


var app = Vue.createApp ({
  data() {
    return {
      skills:[
        {name:"html", experience:"12"},
        {name:"php", experience:"110"},
        {name:"css", experience:"3"},
        {name:"laravel", experience:"5"},
      ],
      newSkills:{name:"", experience:""}
    };
      
    },

    methods:{
      addNew(){
        this.skills.push(this.newSkills)
      },
      remove(i){
        this.skills.splice(i,1);
      }
    }
  
});

app.mount('#app');

  