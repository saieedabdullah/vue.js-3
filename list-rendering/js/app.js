


var app = Vue.createApp ({
  data() {
    return {
     skills: ["html", "css", "vue"],
     newSkill: "",
    };
      
    },

    methods:{
      addNew(){
        this.skills.push(this.newSkill);
        this.newSkill = "";
      },
      removeSkill(i){
        this.skills.splice(i,1);
      }
    }
  
});

app.mount('#app');

  