


var app1 = Vue.createApp ({
  data() {
    return {
      value: ""
    }; 
  }, 
  template: `<input type="text" v-model="value">
  <h2>Value is: {{value}}</h2>`
});

app1.mount('#app1');

var app2 = Vue.createApp ({
  data() {
    return {
      value: "hello"
    }; 
  }, 
  template: "<h2>Value is: {{value}}</h2>" 
});

app2.mount('#app2');

  