


var app = Vue.createApp ({
  data() {
    return {
      show: "" ,
      skills:[
        {name:"html", experience:"5"},
        {name:"php", experience:"10"},
        {name:"css", experience:"3"},
        {name:"laravel", experience:"12"},
        {name:"java", experience:"4"},
      ]
    };
      
    },

    methods:{
      hideShow(){
        this.show = !this.show;
      },
      remove(i){
        this.skills.splice(i,1);
      },
      totalCount(){
        console.log('hi');
        return this.skills.length;
      }
    }
  
});

app.mount('#app');

  